--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.7
-- Dumped by pg_dump version 9.5.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: analitica_ok(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION analitica_ok() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
trobat boolean := False;
query text;
rec record;
count_res int;
estat_analitica boolean :=True;
analitica bigint;
BEGIN
IF TG_TABLE_NAME = 'resultats' THEN
    /* Mira que totes les proves estiguin not null*/
    query := 'select count(*) from resultats where idanalitica = ' || NEW.idanalitica || ' and (resultats = NULL or resultats='''');';
    execute(query) INTO count_res;
    IF count_res = 0 THEN
    /* Mira que no hi hagui res de la analitica a resultats_patologics*/
        query := 'select * from resultats_patologics join resultats on resultats_patologics.idresultat=resultats.idresultat and resultats.idanalitica = '||NEW.idanalitica||';'; 
        FOR rec IN EXECUTE (query) LOOP
            trobat := True;
        END LOOP;
        /* insert */
        IF NOT trobat THEN
            INSERT INTO informes VALUES(NEW.idanalitica, now());
        ELSE 
            RETURN NULL;
        END IF;
    ELSE
        RETURN NULL;
    END IF;
ELSEIF TG_TABLE_NAME = 'resultats_patologics' THEN 
    query := 'select * from resultats_patologics join resultats on resultats_patologics.idresultat=resultats.idresultat and idanalitica in (select idanalitica from resultats where idresultat = '||old.idresultat||') ;';
    FOR rec IN EXECUTE(query) LOOP
      trobat :=True;
    END LOOP;
    
    IF NOT trobat THEN
      query :='select * from resultats where idanalitica  in (select idanalitica from resultats where idresultat = '||old.idresultat||' );';
      FOR rec IN EXECUTE(query) LOOP
        IF rec.resultats IS NULL or rec.resultats = '' THEN
          estat_analitica := False;
          EXIT;
        END IF;
        analitica:=rec.idanalitica;
      END LOOP;
      IF estat_analitica THEN
        INSERT INTO informes VALUES(analitica, now());
      ELSE
        RETURN NULL;
      END IF;
    ELSE
      RETURN NULL;
    END IF;
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.analitica_ok() OWNER TO postgres;

--
-- Name: es_numeric(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION es_numeric(num_str character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    i numeric;
BEGIN
    i := cast (num_str as numeric);
    return true;
EXCEPTION 
    WHEN others then return false;
END 
$$;


ALTER FUNCTION public.es_numeric(num_str character varying) OWNER TO postgres;

--
-- Name: res_pat(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION res_pat() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
valorar varchar;
BEGIN
valorar := valorar_idresultat_plus(NEW.idresultat);
IF valorar = '2' OR valorar = '3' THEN
    INSERT INTO resultats_patologics VALUES (NEW.idresultat, now(), user);
    RETURN NEW;
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.res_pat() OWNER TO postgres;

--
-- Name: restart_dades_plus(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION restart_dades_plus() RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE 
    inserts text;
BEGIN
    execute('DELETE FROM resultats;');
    execute('ALTER SEQUENCE resultats_idresultat_seq RESTART WITH 1');
    execute('DELETE FROM provestecnica;');
    execute('ALTER SEQUENCE provestecnica_idprovatecnica_seq RESTART WITH 1');
    execute('DELETE FROM catalegproves;');
    execute('DELETE FROM analitiques;');
    execute('ALTER SEQUENCE analitiques_idanalitica_seq RESTART WITH 1');
    execute('DELETE FROM doctors;');
    execute('ALTER SEQUENCE doctors_iddoctor_seq RESTART WITH 1');
    execute('DELETE FROM pacients;');
    execute('ALTER SEQUENCE pacients_idpacient_seq RESTART WITH 1');
    execute('DELETE FROM informes;');
    execute('DELETE FROM resultats_patologics;');
    -- INSERTA PACIENTS:
    inserts := 'INSERT INTO pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email, num_ss, num_cat, nie, passaport) VALUES (default, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, NULL, NULL, NULL, NULL);';
    execute(inserts) USING 'jose', 'remar silva', '4811111M', cast('1996-07-12' as date), 'M', 'veciana 8 2 2', 'barcelona', '08023', '989856565', 'jose@mail.com';
    execute(inserts) USING 'anna', 'pons florit', '5811111M', cast('1950-05-11' as date), 'F', 'calle major 43', 'menorca', '07760', '680787787', 'anna@mail.com';
    execute(inserts) USING 'julia', 'ferrer ferrer', '6811111M', cast('1985-01-01' as date), 'F', 'calle menor 74', 'madrid', '78888', '655655655', 'julia@mail.com';

    -- INSERTA DOCTORS: 
    inserts := 'INSERT INTO doctors (iddoctor, nom, cognoms, especialitat) VALUES (default, $1, $2, $3);';
    execute(inserts) USING 'albert', 'marinom', 'cirugia';
    execute(inserts) USING 'maria', 'benavent', 'podologia';
    execute(inserts) USING 'renzo', 'remar', 'fisioterapia';

    -- INSERTA ANALITIQUES:
    inserts := 'INSERT INTO analitiques (idanalitica, iddoctor, idpacient, dataanalitica) VALUES (default, $1, $2, CURRENT_TIMESTAMP);';
    execute(inserts) USING 1, 2;
    execute(inserts) USING 1, 1;
    execute(inserts) USING 2, 1;
    execute(inserts) USING 3, 1;
    execute(inserts) USING 3, 2;
    execute(inserts) USING 3, 3;
    execute(inserts) USING 1, 3;
    execute(inserts) USING 1, 2;


    -- INSERTA CATALEG PROVES
    inserts := 'INSERT INTO catalegproves (idprova, nom_prova, descripcio,acronim, nivellalarma, missatgealarma) VALUES ($1, $2, $3, $4, $5, $6);';
    execute(inserts) USING 100, 'glucosa', 'Prueba de la glucosa', 'GLU',0,'Pos en glucosa';
    execute(inserts) USING 200, 'cocaina', 'Prueba de la cocaina', 'COC',1,'Pos en cocaina';
    execute(inserts) USING 300, 'colesterol', 'Prueba del colesterol', 'COL',0,'Pos en colesterol';
    execute(inserts) USING 400, 'vih', 'prueba de vih', 'VIH',3,'Pos en VIH';

    -- INSERTA PROVES TECNICA (diferents versions per una prova. Data prova es a partir de quan es crea aquesta nova tecnica. Sexe no ho empram de moment)
    inserts := 'INSERT INTO provestecnica (idprovatecnica, idprova, sexe, edatInicial, edatFinal, dataprova, resultat_numeric, minpat, maxpat, minpan, maxpan, alfpat) VALUES (default, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);';
    execute(inserts) USING 100, 1,0,999, cast('2012-10-01 00:00:00' as timestamp), true, 80, 100, 60, 150, '';
    execute(inserts) USING 100, 2,0,999, cast('2013-01-01 00:00:00' as timestamp), true, 70, 90, 50, 110, '';
    execute(inserts) USING 200, 1,0,999, cast('2014-05-01 00:00:00' as timestamp), true, 131, 170, 100, 190, '';
    execute(inserts) USING 200, 2,0,999, cast('2012-01-01 00:00:00' as timestamp), true, 81, 120, 50, 140, '';
    execute(inserts) USING 300, 1,0,1, cast('2017-03-20 00:00:00' as timestamp), true, 132, 171, 101, 191, '';
    execute(inserts) USING 300, 1,1,4, cast('2011-08-12 00:00:00' as timestamp), true, 83, 122, 52, 142, '';
    execute(inserts) USING 300, 1,4,999, cast('2010-12-22 00:00:00' as timestamp), true, 115, 154, 84, 174, '';
    execute(inserts) USING 300, 2,0,1, cast('2017-03-20 00:00:00' as timestamp), true, 132, 171, 101, 191, '';
    execute(inserts) USING 300, 2,1,4, cast('2011-08-12 00:00:00' as timestamp), true, 83, 122, 52, 142, '';
    execute(inserts) USING 300, 2,4,999, cast('2010-12-22 00:00:00' as timestamp), true, 115, 154, 84, 174, '';
    execute(inserts) USING 400, 0,0,999, cast('2018-01-01 00:00:00' as timestamp), false, 0, 0, 0, 0, 'NEG';

    -- INSERTA RESULTATS
    inserts := 'INSERT INTO resultats (idresultat, idanalitica, idprovatecnica, resultats, dataresultat) VALUES (default, $1, $2, $3, CURRENT_TIMESTAMP);';
    execute(inserts) USING 1, 2, '80';
    execute(inserts) USING 1, 4, '90';
    execute(inserts) USING 2, 1, '80';
    execute(inserts) USING 2, 3, '100';
    execute(inserts) USING 3, 7, '200';
    execute(inserts) USING 3, 1, '150';
    execute(inserts) USING 7, 11, 'POS';
    execute(inserts) USING 8, 11, 'NEG';

RETURN '0';
EXCEPTION 
  WHEN not_null_violation THEN return 'ERROR: NOT NULL VIOLATION'; 
  WHEN unique_violation THEN return 'ERROR: UNIQUE KEY VIOLATON'; 
  --WHEN foreign_key_violation THEN return 'CLAU FORANA INEXISTENT';  
  WHEN check_violation THEN return 'ERROR: CHECK VIOLATION';
  --WHEN others THEN return 'ANOTHER ERROR';
END;
$_$;


ALTER FUNCTION public.restart_dades_plus() OWNER TO postgres;

--
-- Name: valorar_idresultat_plus(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION valorar_idresultat_plus(idresultat bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    query text;
    query_1 text;
    resultat_select record;
    resultat_select_1 text;
    res_valoracio varchar;
BEGIN

    query := 'SELECT idprovatecnica, idanalitica, resultats FROM resultats WHERE idresultat = ' || idresultat || ';';
    execute(query) INTO resultat_select;
    IF resultat_select IS NULL THEN
        RETURN '-1';
    END IF;


    query_1 := 'SELECT idpacient FROM analitiques WHERE idanalitica = ' || resultat_select.idanalitica || ';';
    execute(query) INTO resultat_select_1;
    IF resultat_select_1 IS NULL THEN
        RETURN '-1';
    END IF;

    res_valoracio := valorar_res_plus(resultat_select.idprovatecnica, cast(resultat_select_1 as int), resultat_select.resultats);

RETURN res_valoracio;
EXCEPTION 
  WHEN not_null_violation THEN return 'ERROR: NOT NULL VIOLATION'; 
  WHEN unique_violation THEN return 'ERROR: UNIQUE KEY VIOLATON'; 
  WHEN foreign_key_violation THEN return 'CLAU FORANA INEXISTENT';  
  WHEN check_violation THEN return 'ERROR: CHECK VIOLATION';
  WHEN others THEN return 'ANOTHER ERROR';
END;
$$;


ALTER FUNCTION public.valorar_idresultat_plus(idresultat bigint) OWNER TO postgres;

--
-- Name: valorar_res_plus(bigint, bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION valorar_res_plus(idprovatecnica bigint, pacient bigint, resultat character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$
/* COMPROVACIO 
select valorar_res_plus(1,2,'POS');
 valorar_res_plus 
------------------
 3
(1 row)
*/
DECLARE 
    trobat boolean;
    resultat_select record;
    query text;
    resultat_int int;
BEGIN
    query := 'SELECT resultat_numeric, minpat, maxpat, minpan, maxpan, alfpat FROM provestecnica WHERE idprovatecnica = ''' || idprovatecnica || ''';';
    execute(query) INTO resultat_select; --comprovem que el select ha agafat el registre i que existeix
    IF resultat_select IS NULL THEN
        RETURN '-1';
    END IF;

    IF resultat_select.resultat_numeric THEN
            
        IF es_Numeric(resultat) THEN

            resultat_int := cast(resultat as int);
            IF resultat_int < resultat_select.minpan THEN
                RETURN '3';
            ELSIF resultat_int >  resultat_select.maxpan THEN
                RETURN '3';
            ELSIF resultat_int < resultat_select.minpat THEN
                RETURN '2';
            ELSEIF resultat_int > resultat_select.maxpat THEN
                RETURN '2';
            END IF;
        ELSE
            RETURN '3';
        END IF;
    ELSE
        IF resultat_select.alfpat != resultat THEN
            RETURN '2';
        END IF;

    END IF;

RETURN '1';
EXCEPTION 
  WHEN not_null_violation THEN return 'ERROR: NOT NULL VIOLATION'; 
  WHEN unique_violation THEN return 'ERROR: UNIQUE KEY VIOLATON'; 
  WHEN foreign_key_violation THEN return 'CLAU FORANA INEXISTENT';  
  WHEN check_violation THEN return 'ERROR: CHECK VIOLATION';
  --WHEN others THEN return 'ANOTHER ERROR';
END;
$$;


ALTER FUNCTION public.valorar_res_plus(idprovatecnica bigint, pacient bigint, resultat character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alarmes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE alarmes (
    idalarma integer NOT NULL,
    idresultat bigint,
    nivellalarma smallint NOT NULL,
    validat boolean NOT NULL,
    missatge character varying(100) NOT NULL,
    tsalarma timestamp without time zone NOT NULL
);


ALTER TABLE alarmes OWNER TO postgres;

--
-- Name: alarmes_idalarma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alarmes_idalarma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmes_idalarma_seq OWNER TO postgres;

--
-- Name: alarmes_idalarma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alarmes_idalarma_seq OWNED BY alarmes.idalarma;


--
-- Name: analitiques; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE analitiques (
    idanalitica integer NOT NULL,
    iddoctor bigint,
    idpacient bigint,
    dataanalitica timestamp without time zone NOT NULL
);


ALTER TABLE analitiques OWNER TO postgres;

--
-- Name: analitiques_idanalitica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE analitiques_idanalitica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analitiques_idanalitica_seq OWNER TO postgres;

--
-- Name: analitiques_idanalitica_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE analitiques_idanalitica_seq OWNED BY analitiques.idanalitica;


--
-- Name: catalegproves; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalegproves (
    idprova integer NOT NULL,
    nom_prova character varying(15) NOT NULL,
    descripcio character varying(100) NOT NULL,
    acronim character varying(15),
    nivellalarma smallint,
    missatgealarma character varying(200),
    prova_perpetua boolean DEFAULT false
);


ALTER TABLE catalegproves OWNER TO postgres;

--
-- Name: doctors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE doctors (
    iddoctor integer NOT NULL,
    nom character varying(15) NOT NULL,
    cognoms character varying(30) NOT NULL,
    especialitat character varying(30) NOT NULL
);


ALTER TABLE doctors OWNER TO postgres;

--
-- Name: doctors_iddoctor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE doctors_iddoctor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE doctors_iddoctor_seq OWNER TO postgres;

--
-- Name: doctors_iddoctor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE doctors_iddoctor_seq OWNED BY doctors.iddoctor;


--
-- Name: informes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE informes (
    idanalitica bigint NOT NULL,
    stamp timestamp without time zone NOT NULL
);


ALTER TABLE informes OWNER TO postgres;

--
-- Name: pacients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pacients (
    idpacient integer NOT NULL,
    nom character varying(15) NOT NULL,
    cognoms character varying(30) NOT NULL,
    dni character varying(9),
    data_naix date NOT NULL,
    sexe character varying(1) NOT NULL,
    adreca character varying(50) NOT NULL,
    ciutat character varying(30) NOT NULL,
    c_postal character varying(10) NOT NULL,
    telefon character varying(9) NOT NULL,
    email character varying(30) NOT NULL,
    num_ss character varying(12),
    num_cat character varying(20),
    nie character varying(20),
    passaport character varying(20)
);


ALTER TABLE pacients OWNER TO postgres;

--
-- Name: pacients_idpacient_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pacients_idpacient_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pacients_idpacient_seq OWNER TO postgres;

--
-- Name: pacients_idpacient_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pacients_idpacient_seq OWNED BY pacients.idpacient;


--
-- Name: prova1; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW prova1 AS
 SELECT doctors.nom
   FROM doctors;


ALTER TABLE prova1 OWNER TO postgres;

--
-- Name: provestecnica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE provestecnica (
    idprovatecnica integer NOT NULL,
    idprova integer,
    sexe integer NOT NULL,
    edatinicial integer NOT NULL,
    edatfinal integer NOT NULL,
    dataprova timestamp without time zone NOT NULL,
    resultat_numeric boolean DEFAULT true NOT NULL,
    minpat double precision NOT NULL,
    maxpat double precision NOT NULL,
    minpan double precision NOT NULL,
    maxpan double precision NOT NULL,
    alfpat character varying(10)
);


ALTER TABLE provestecnica OWNER TO postgres;

--
-- Name: provestecnica_idprovatecnica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE provestecnica_idprovatecnica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE provestecnica_idprovatecnica_seq OWNER TO postgres;

--
-- Name: provestecnica_idprovatecnica_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE provestecnica_idprovatecnica_seq OWNED BY provestecnica.idprovatecnica;


--
-- Name: resultats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE resultats (
    idresultat integer NOT NULL,
    idanalitica bigint,
    idprovatecnica bigint,
    resultats character varying(10),
    dataresultat timestamp without time zone NOT NULL
);


ALTER TABLE resultats OWNER TO postgres;

--
-- Name: resultats_idresultat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE resultats_idresultat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resultats_idresultat_seq OWNER TO postgres;

--
-- Name: resultats_idresultat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE resultats_idresultat_seq OWNED BY resultats.idresultat;


--
-- Name: resultats_patologics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE resultats_patologics (
    idresultat integer NOT NULL,
    stamp timestamp without time zone NOT NULL,
    userid text NOT NULL
);


ALTER TABLE resultats_patologics OWNER TO postgres;

--
-- Name: idalarma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alarmes ALTER COLUMN idalarma SET DEFAULT nextval('alarmes_idalarma_seq'::regclass);


--
-- Name: idanalitica; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY analitiques ALTER COLUMN idanalitica SET DEFAULT nextval('analitiques_idanalitica_seq'::regclass);


--
-- Name: iddoctor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY doctors ALTER COLUMN iddoctor SET DEFAULT nextval('doctors_iddoctor_seq'::regclass);


--
-- Name: idpacient; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacients ALTER COLUMN idpacient SET DEFAULT nextval('pacients_idpacient_seq'::regclass);


--
-- Name: idprovatecnica; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provestecnica ALTER COLUMN idprovatecnica SET DEFAULT nextval('provestecnica_idprovatecnica_seq'::regclass);


--
-- Name: idresultat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultats ALTER COLUMN idresultat SET DEFAULT nextval('resultats_idresultat_seq'::regclass);


--
-- Data for Name: alarmes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alarmes (idalarma, idresultat, nivellalarma, validat, missatge, tsalarma) FROM stdin;
\.


--
-- Name: alarmes_idalarma_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alarmes_idalarma_seq', 1, false);


--
-- Data for Name: analitiques; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY analitiques (idanalitica, iddoctor, idpacient, dataanalitica) FROM stdin;
1	1	2	2018-03-14 08:32:20.714965
2	1	1	2018-03-14 08:32:20.714965
3	2	1	2018-03-14 08:32:20.714965
4	3	1	2018-03-14 08:32:20.714965
5	3	2	2018-03-14 08:32:20.714965
6	3	3	2018-03-14 08:32:20.714965
7	1	3	2018-03-14 08:32:20.714965
8	1	2	2018-03-14 08:32:20.714965
9	1	2	2018-04-05 11:58:39.637536
\.


--
-- Name: analitiques_idanalitica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('analitiques_idanalitica_seq', 8, true);


--
-- Data for Name: catalegproves; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY catalegproves (idprova, nom_prova, descripcio, acronim, nivellalarma, missatgealarma, prova_perpetua) FROM stdin;
100	glucosa	Prueba de la glucosa	GLU	0	Pos en glucosa	f
200	cocaina	Prueba de la cocaina	COC	1	Pos en cocaina	f
300	colesterol	Prueba del colesterol	COL	0	Pos en colesterol	f
400	vih	prueba de vih	VIH	3	Pos en VIH	t
\.


--
-- Data for Name: doctors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY doctors (iddoctor, nom, cognoms, especialitat) FROM stdin;
1	albert	marinom	cirugia
2	maria	benavent	podologia
3	renzo	remar	fisioterapia
4	adri	davila	fisioterapia
\.


--
-- Name: doctors_iddoctor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('doctors_iddoctor_seq', 4, true);


--
-- Data for Name: informes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY informes (idanalitica, stamp) FROM stdin;
1	2018-03-14 08:32:20.714965
1	2018-03-14 08:32:20.714965
2	2018-03-14 08:32:20.714965
2	2018-03-14 08:32:20.714965
3	2018-03-14 08:32:20.714965
7	2018-03-14 08:32:20.714965
8	2018-03-14 08:32:20.714965
\.


--
-- Data for Name: pacients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email, num_ss, num_cat, nie, passaport) FROM stdin;
1	jose	remar silva	4811111M	1996-07-12	M	veciana 8 2 2	barcelona	08023	989856565	jose@mail.com	\N	\N	\N	\N
2	anna	pons florit	5811111M	1950-05-11	F	calle major 43	menorca	07760	680787787	anna@mail.com	\N	\N	\N	\N
3	julia	ferrer ferrer	6811111M	1985-01-01	F	calle menor 74	madrid	78888	655655655	julia@mail.com	\N	\N	\N	\N
\.


--
-- Name: pacients_idpacient_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pacients_idpacient_seq', 3, true);


--
-- Data for Name: provestecnica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY provestecnica (idprovatecnica, idprova, sexe, edatinicial, edatfinal, dataprova, resultat_numeric, minpat, maxpat, minpan, maxpan, alfpat) FROM stdin;
1	100	1	0	999	2012-10-01 00:00:00	t	80	100	60	150	
2	100	2	0	999	2013-01-01 00:00:00	t	70	90	50	110	
3	200	1	0	999	2014-05-01 00:00:00	t	131	170	100	190	
4	200	2	0	999	2012-01-01 00:00:00	t	81	120	50	140	
5	300	1	0	1	2017-03-20 00:00:00	t	132	171	101	191	
6	300	1	1	4	2011-08-12 00:00:00	t	83	122	52	142	
7	300	1	4	999	2010-12-22 00:00:00	t	115	154	84	174	
8	300	2	0	1	2017-03-20 00:00:00	t	132	171	101	191	
9	300	2	1	4	2011-08-12 00:00:00	t	83	122	52	142	
10	300	2	4	999	2010-12-22 00:00:00	t	115	154	84	174	
11	400	0	0	999	2018-01-01 00:00:00	f	0	0	0	0	NEG
\.


--
-- Name: provestecnica_idprovatecnica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('provestecnica_idprovatecnica_seq', 11, true);


--
-- Data for Name: resultats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY resultats (idresultat, idanalitica, idprovatecnica, resultats, dataresultat) FROM stdin;
1	1	2	80	2018-03-14 08:32:20.714965
2	1	4	90	2018-03-14 08:32:20.714965
3	2	1	80	2018-03-14 08:32:20.714965
4	2	3	100	2018-03-14 08:32:20.714965
5	3	7	200	2018-03-14 08:32:20.714965
6	3	1	150	2018-03-14 08:32:20.714965
7	7	11	POS	2018-03-14 08:32:20.714965
8	8	11	NEG	2018-03-14 08:32:20.714965
\.


--
-- Name: resultats_idresultat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('resultats_idresultat_seq', 8, true);


--
-- Data for Name: resultats_patologics; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY resultats_patologics (idresultat, stamp, userid) FROM stdin;
4	2018-03-14 08:32:20.714965	postgres
5	2018-03-14 08:32:20.714965	postgres
6	2018-03-14 08:32:20.714965	postgres
7	2018-03-14 08:32:20.714965	postgres
\.


--
-- Name: alarmes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alarmes
    ADD CONSTRAINT alarmes_pkey PRIMARY KEY (idalarma);


--
-- Name: analitiques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY analitiques
    ADD CONSTRAINT analitiques_pkey PRIMARY KEY (idanalitica);


--
-- Name: catalegproves_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalegproves
    ADD CONSTRAINT catalegproves_pkey PRIMARY KEY (idprova);


--
-- Name: doctors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY doctors
    ADD CONSTRAINT doctors_pkey PRIMARY KEY (iddoctor);


--
-- Name: pacients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pacients
    ADD CONSTRAINT pacients_pkey PRIMARY KEY (idpacient);


--
-- Name: provestecnica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provestecnica
    ADD CONSTRAINT provestecnica_pkey PRIMARY KEY (idprovatecnica, sexe, edatinicial);


--
-- Name: resultats_idanalitica_idprovatecnica_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultats
    ADD CONSTRAINT resultats_idanalitica_idprovatecnica_key UNIQUE (idanalitica, idprovatecnica);


--
-- Name: resultats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultats
    ADD CONSTRAINT resultats_pkey PRIMARY KEY (idresultat);


--
-- Name: informe_res; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER informe_res AFTER INSERT OR UPDATE ON resultats FOR EACH ROW EXECUTE PROCEDURE analitica_ok();


--
-- Name: queden_resultats; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER queden_resultats AFTER DELETE ON resultats_patologics FOR EACH ROW EXECUTE PROCEDURE analitica_ok();


--
-- Name: res_pat; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER res_pat AFTER INSERT OR UPDATE ON resultats FOR EACH ROW EXECUTE PROCEDURE res_pat();


--
-- Name: fk_idanaliticainforme; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informes
    ADD CONSTRAINT fk_idanaliticainforme FOREIGN KEY (idanalitica) REFERENCES analitiques(idanalitica) ON UPDATE CASCADE;


--
-- Name: fk_iddoctor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY analitiques
    ADD CONSTRAINT fk_iddoctor FOREIGN KEY (iddoctor) REFERENCES doctors(iddoctor) ON UPDATE CASCADE;


--
-- Name: fk_idpacient; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY analitiques
    ADD CONSTRAINT fk_idpacient FOREIGN KEY (idpacient) REFERENCES pacients(idpacient) ON UPDATE CASCADE;


--
-- Name: fk_idprova; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY provestecnica
    ADD CONSTRAINT fk_idprova FOREIGN KEY (idprova) REFERENCES catalegproves(idprova) ON UPDATE CASCADE;


--
-- Name: fk_idresultatpat; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultats_patologics
    ADD CONSTRAINT fk_idresultatpat FOREIGN KEY (idresultat) REFERENCES resultats(idresultat) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

