#! /bin/bash
# Comandes per actualitzar dcoker i utilitzar docker compose, docker-machine i VirtualBox.
# Esborrar versió actual de docker
dnf -y remove docker
# També pot ser docker-ce
# Install Docker
dnf -y install dnf-plugins-core
dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
dnf config-manager --set-enabled docker-ce-edge
dnf config-manager --set-enabled docker-ce-test
dnf -y install docker-ce
# Comprovació
echo "Versió de Docker"
echo "----------------"
docker --version
echo "----------------"
# Install Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
# Comprovació
echo "Versió de Docker Compose"
echo "----------------"
docker-compose --version
echo "----------------"
# Install Docker Machine
base=https://github.com/docker/machine/releases/download/v0.14.0 && curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine
echo "Versió de Docker Machine"
echo "----------------"
docker-machine version
echo "----------------"
# Install VirtualBox
wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo -O /etc/yum.repos.d/virtualbox.repo
dnf -y install VirtualBox-5.1
# Kernel per que VirtualBox funcioni sense errors
dnf install -y kernel-headers kernel-devel dkms gcc
/sbin/vboxconfig