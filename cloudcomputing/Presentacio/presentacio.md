% Cloud Computing
% Adrián Dávila Montañez
% 2018/05/22

---

# CloudComputing

La definició tècnica seria que la informàtica en el núvol és el lliurament sota demanda de potència informàtica, emmagatzematge en bases de dades, 
aplicacions i altres recursos de TI a través d'Internet amb un sistema de preus basat en el consum realitzat.	

* Basat en:

	* Recursos compartits
	
	* Programari al núvol
	
---

# Containers

### Què són?

Espais aïllats generats pel kernel linux que permeten l'execució d'aplicacions aïllades entre si. 

### Utilitat

Utilitzats en major part per a entorns de proves, d'un nou sistema operatiu, una nova configuració per al teu servidor web, etc. ja que la seva producció és molt més ràpida i ocupa menys espai que una màquina virtual.

---

![Docker vs VM](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/dockervsvm.png)

---

# Docker

Docker és la companyia que porta el moviment dels containers i l'únic proveïdor de plataforma de contenidors per fer front a totes les aplicacions a través del núvol híbrid. 

L'objectiu és eliminar el problema anomenat _funciona a la meva màquina_.

#### Núvol

Les imatges utilitzades per a crear els serveis estan allotjades a Docker Hub i els containers es generen automàticament segons la configuració del stack.

--- 

# Serveis

Un servei és la imatge d'un servei de micro servei en el context d'una aplicació més gran. 

Per tant, cada servei que incloem al Docker Compose, generarà 1 o N tasques.

Podríem dir que tasques és el mateix que repliques.

* Llista dels serveis:
    * PostgreSQL
    * Openldap
    * Visualizer
    * Portainer

---

![Services](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/diagramaserveis.png)

---

# Docker Compose

Docker Compose és una eina Docker per a definir i executar aplicacions multi-container. Compose utilitza un fitxer YAML per a definir els serveis que volem crear dins de la nostra aplicació.

Tenint aquest fitxer i amb una sola comanda, farem que la nostra aplicació començi a funcionar (si tot está ben configurat, és clar!).

* En entorns de desenvolupament

* Entorns de proves automàtics

---

# Fitxer Compose

El fitxer Compose defineix Serveis, Xarxes i Volums. 
La forma més ràpida per crear stacks i swarms és utilitzant un fitxer YAML amb la configuració dels serveis.

Cada definició de servei, amb la seva configuració, afecta els containers generats automàticament per a assegurar l'execució d'aquest. 

És a dir, fa els _docker container create_ necessaris per a posar-se en marxa.

---

# Docker Swarm

* Un Swarm és un conjunt de màquines que executen Docker i que formen un clúster. 

* Les ordres docker són les mateixes però ara aniran dirigides al Swarm manager. 

* Les màquines d'un swarm poden ser físiques o virtuals. 

* Després d'unir-se a un swarm, les màquines es denominen nodes.

* Els nodes workers serveixen per a donar capacitat, ja que no tenen cap autoritat per autoritzar res.

--- 

# Routing Mesh

El routing mesh fa que cada node pertanyent al swarm accepti connexions als ports publicats per a qualsevol servei funcionant al swarm, malgrat que el node no tingui una tasca funcionant en el dit node. 

Routing Mesh encamina totes les peticions entrants als ports publicats cap a un container actiu.

És la característica principal pel qual un swarm té sentit; el fet de no tenir una tasca executant en el propi node però poder donar servei.

---

# Docker Machine

És una eina Docker que permet la instal·lació del motor Docker en màquines virtuals que resideixen al teu host, al cloud o a servers físics. 

Aquestes màquines són controlades, i creades, amb les ordres derivades de docker-machine.

Per tant amb Docker Machine s'habilita la possibilitat de crear imatges i containers a un entorn probablement de proves com pot ser una màquina virtual.

---

# Proveidors Cloud

## Amazon Web Services

És una filial d'Amazon que proporciona plataformes de computació en xarxa sota demanda.
 
La tecnologia permet als subscriptors disposar d'un ampli clúster virtual d'ordinadors, disponible tot el temps, a través d'Internet. 

Les màquines virtuals que proporciona AWS s'anomenen instàncies.

---

# Tipus de Proveidors

Quin ha de ser el detall que ens faci escollir un proveïdor o un altre? 
Doncs molt senzill, segons la utilitat que vulguis donar, els recursos que necessites i el cost que pots assumir.

Llavors, hi ha dos grans grups els quals ens podem ajudar a escollir: 

* Propòsits generals

* Costos reduïts.

---

# Propòsits generals

Un núvol de propòsit general està destinat a executar qualsevol cosa. Pot substituir un bastidor complet de servidors.

Què ens pot fer saber que és el nostre cas? Si contestaríem si a qualsevol d'aquestes preguntes:

* Executes més de 50 màquines virtuals?
* Estàs gastant més de 1000 dòlars / mes en allotjament?
* La vostra infraestructura abasta diversos centres de dades?

En aquest àmbit hi juguen empreses com AWS, Google Cloud i Microsoft Azure com a grans destacats.

### Selecció

---

# Costos reduïts

Un núvol de costos reduïts està destinat a oferir un maquinari decent i una bona connectivitat a Internet, a un preu assequible. 

* Fàcil de gestionar
* Bons resultats

Què ens pot fer saber que és el nostre cas? Si contestaríem si a qualsevol d'aquestes preguntes:

* Executes menys de 5 màquines virtuals?
* Estàs gastant menys de 100 dòlars / mes en allotjament?
* T'etiquetaries com a amateur o aficionat?

### Selecció

---

# Exemple pràctic

## Swarm amb 2 Instancies AWS EC2
 
## Amazon Web Services Elastic Computing Cloud

---

![End](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/fi.jpg)
