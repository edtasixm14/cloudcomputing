#! /bin/bash
# Nom: Adrián Dávila Montañez
# Curs: hisx2
# Sinopsys: Script per pasar un fitxer de extensió md a html.
#----------------------------------------------------------

pandoc \
	--standalone \
	--to=dzslides \
	--incremental \
	--css=html.css \
	--output=presentacio.html \
presentacio.md