# Dietari projecte "cloudcomputing".

## 19-04-2018
L'objectiu de l'estona de treball avui era deixar preparat el repositori per a començar dilluns.
També enviar a tots els professors la direcció del repositori per correu.

Objectiu assolit.

## 23-04-2018

Fer almenys els dos primers punts del getstarted de Docker. Orientation i Containers.

Objectiu assolit, he arribat fins a al punt 4 creant les [VM](https://docs.docker.com/get-started/part4/#create-a-cluster), per tant he fet tambe la part 3: Services i començant Swarms 

## 25-04-2018

Acabar el getStarted de Docker ( Swarms i Stacks )

He tingut un parell de problemes amb l'stack que no m'iniciaba pero l'he finalitzat i esborrat correctament.

## 26-04-2018 

Crear un Dockerfile per a un server ldap deattached i comprovar funcionament en un swarm al host.
* Dockerfile ok + espose ports + docker-compose (docker swarm init + docker stack deploy -c dockercomposefile.yml nomapp) OK
* Comprovacio a vm's KO

## 27-04-2018 

Crear compte a AWS i Docker Cloud

Fet i a més he linkat un swarm fet AWS amb Docker Cloud.

## 02-05-2018

Accedir amb ip/hostname d'AWS als serveis creats amb Docker-Compose. Generar un stack dins del swarm hostejat per AWS.

Fet.


## 03-05-2018

Trobar una solució a:
* Flapping de les instancies de AWS que donen servei LDAP.
* Conexió SSH a la màquina d'AWS.

M'he quedat estancat en aquest punt i he passat a fer el dockerfile per crear l'imatge de postgres. Succesfully.

## 04-05-2018

Crear un docker-compose per a PSQL, crear un stack amb el compose, consultar el visualizer.

Succesfully.

## 07-05-2018
## 08-05-2018
## 09-05-2018

No he trobat solució al flapping, sé quan funciona i quan no pero no sé el perquè. La setmana que ve ho faré amb Digital Ocean. 

He passat a fer documentació, estudiar i aprendre sobre Compose, Machine i Swarm per preparar la presentació.

## 10-05-2018
## 11-05-2018
## 14-05-2018

Canvi de plans respecte al dia 3. Eliminació dels swarms creats amb Docker Cloud. 

Preparació del swarm local, amb comprovacions. Creació de 2 instancies AWS per a fer SwarmCloud. Repàs dels documents per a bona presentació.

## 15-05-2018

Fer que el Swarm Cloud funcioni com cal, continuar arreglant documents i començar a acabar la explicació de CloudComputing (presentació). 

Per alguna raó no detecta la xarxa ingress per fer routing mesh.

## 16-05-2018

Trobar solució a swarm perque detecti el routing mesh.

FET!

## 17-05-2018

Revisió dels documents, correcció ortogràfica.

## 18-05-2018

Preparació presentació (md per a generar un html que sigui el _power point_ per a presentar.)

## 19-05-2018
## 20-05-2018
## 21-05-2018

Practicar presentació. Últims detalls a documentació.