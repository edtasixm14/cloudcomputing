# AWS

Hi, on this document we will create a single Amazon Linux AMI to play with it, deploy some services (and test them) with docker engine.

First of all, you have to create a AWS account. Can follow some steps from:
* [Amazon tutorial](https://aws.amazon.com/es/premiumsupport/knowledge-center/create-and-activate-aws-account/)

### Now we'll create a free tier instance:
* On left menu, select **Instances**.
* Pick "Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type - ami-c12dcda6" and click "Select".
* Default instance configuration isn't enough for our purposes. It open ssh from everywhere. Now we need to open traffic to 9000, 8080, 389 and 5432.


![Security Rules](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/securityrules.png)

* Click "Launch".
    * If you don't have any key just create a new one, name and download it. Then you will be able to "Launch Instances".
* Name instance on "Name" column and wait till instance state changes to **running**.


![AWS Instance](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/awsinstance.png)

* As you can see on image, click on "Connect" button and follow steps. Remember to:
    * Locate you sshkey path.
    * Change permissions of sshkey to 400.

### Log in:

    [isx48102233@i11 cloudcomputing]$ chmod 400 ~/Downloads/fromi11.pem 
    [isx48102233@i11 cloudcomputing]$ ssh -i "~/Downloads/fromi11.pem" ec2-user@ec2-172-31-29-231.eu-west-2.compute.amazonaws.com
    
### Amazon recommends you to update, run the following:

    sudo yum [ec2-user@ip-172-31-29-231 ~]$ sudo yum update
    
### By default, docker is not installed, let's install it:

    [ec2-user@ip-172-31-29-231 ~]$ sudo yum install docker
    [ec2-user@ip-172-31-29-231 ~]$ sudo service docker start
    Starting cgconfig service:                                 [  OK  ]
    Starting docker:	                                       [  OK  ]
    [ec2-user@ip-172-31-29-231 ~]$ sudo usermod -aG docker ec2-user
    [ec2-user@ip-172-31-29-231 ~]$ docker version
    Client:
    Version:	17.12.1-ce
    
### Install Docker Compose:

    [ec2-user@ip-172-31-29-231 ~]$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0   1111      0 --:--:-- --:--:-- --:--:--  1111
    100 10.3M  100 10.3M    0     0  1535k      0  0:00:06  0:00:06 --:--:-- 2282k
    [ec2-user@ip-172-31-29-231 ~]$ sudo chmod +x /usr/local/bin/docker-compose
    [ec2-user@ip-172-31-29-231 ~]$ docker-compose --version
    docker-compose version 1.21.0, build 5920eb0

### Install Docker Machine:

    [ec2-user@ip-172-31-29-231 ~]$ base=https://github.com/docker/machine/releases/download/v0.14.0 && sudo curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0   1287      0 --:--:-- --:--:-- --:--:--  1285
    100 26.7M  100 26.7M    0     0  1589k      0  0:00:17  0:00:17 --:--:-- 1893k
    [ec2-user@ip-172-31-29-231 ~]$ docker-machine version
    docker-machine version 0.14.0, build 89b8332

### Install VirtualBox:

There is a problem with VirtualBox in a AWS Instance. We can't "virtualize" on a virtual machine. AWS instance is already a virtual machine so we can't build a virtual machine inside a virtual machine.
    
### Prepare the machine:
* Install ldapclient to test
    * [ec2-user@ip-172-31-29-231 ~]$ sudo yum install openldap-clients -y
* Install postgres to test
    * [ec2-user@ip-172-31-29-231 ~]$ sudo yum install postgresql -y

### Deploy the app with 4 services:
* Ldapserver
* PostgreSQL
* Portainer
* Visualizer

#### Create the swarm and deploy app:

    [ec2-user@ip-172-31-29-231 ~]$ docker swarm init
    Swarm initialized: current node (x36zura7p19ilac9027r5ngxy) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-4npz9az6b7imklgh3xvxuszkgb96brutm0g0qowwxcvhvk1oy3-48cdb0en35kn2lizyzcptlnye 172.31.29.231:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

    [ec2-user@ip-172-31-29-231 ~]$ docker stack deploy -c docker-compose-cloud.yml AmazonCloud
    Creating network AmazonCloud_netadri
    Creating network AmazonCloud_default
    Creating service AmazonCloud_psql
    Creating service AmazonCloud_visualizer
    Creating service AmazonCloud_portainer
    Creating service AmazonCloud_ldap

    [ec2-user@ip-172-31-29-231 ~]$ docker stack ls
    NAME                SERVICES
    AmazonCloud         4

    [ec2-user@ip-172-31-29-231 ~]$ docker stack ps AmazonCloud
    ID                  NAME                       IMAGE                             NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
    qke06g679ufo        AmazonCloud_ldap.1         xdri97/ldapserver:cloud           ip-172-31-29-231    Running             Running 11 seconds ago                       
    5f2tcwkfmch8        AmazonCloud_portainer.1    portainer/portainer:latest        ip-172-31-29-231    Running             Running 36 seconds ago                       
    rpp9p7nybda5        AmazonCloud_visualizer.1   dockersamples/visualizer:stable   ip-172-31-29-231    Running             Running 41 seconds ago                       
    y9vyh2cszobt        AmazonCloud_psql.1         xdri97/postgresql:cloud           ip-172-31-29-231    Running             Running 43 seconds ago                       
    lddwayexu2cf        AmazonCloud_ldap.2         xdri97/ldapserver:cloud           ip-172-31-29-231    Running             Running 11 seconds ago                       
    p0f3akev80dw        AmazonCloud_psql.2         xdri97/postgresql:cloud           ip-172-31-29-231    Running             Running 43 seconds ago 

### Test that machine accepts psql and ldapsearch commands on localhost:

    [ec2-user@ip-172-31-29-231 ~]$ psql -U docker -h localhost -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

    [ec2-user@ip-172-31-29-231 ~]$ ldapsearch -x -LLL -h localhost -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

### Now the fire test. Ask to public IP of AWS instance from your host:

    [root@i24 ~]# ldapsearch -x -LLL -h 35.178.86.196 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

    [root@i24 ~]# psql -U docker -h 35.178.86.196 -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

##### IT WORKS!

You can also test the other 2 services, Portainer and Visualizer with Instance Public IP:

### This is the Dashboard of Portainer service:

![Portainer Dashboard](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/dashboardportainer.png )

### You can also see the all services with this one:

![Portainer Services](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/servicesportainer.png)

### And finally the Visualizer service also works:

![Visualizer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/visualizer.png)

## Thank you for paying attention and good luck with your instance!

**Important note: Everytime you stop and start the instance on AWS it will give the instance a different IP. You can pay if you want to get always the same one.** 