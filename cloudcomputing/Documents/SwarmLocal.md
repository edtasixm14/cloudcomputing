Hi! I will mount my own swarm using 4 different machines. They will be:
* My own host (i24) as Manager.
* worker1 as worker.
* worker2 as worker.
* Another physical host (i18) as worker.

So i (and everyone who wants) can create a swarm using both physical and virtual machines.

* First of all make sure that both physical hosts have updated for this task, just as i made using [this](https://gitlab.com/isx48102233/cloudcomputing/blob/master/updates.sh) script.
* Make sure that you are talking to correct host using *eval $(docker-machine env -u)* to remove connections if they exists.

Create 2 vm's on own host. They will be workers so name it with something that identifies them.

    [root@i24 ~]# docker-machine create --driver virtualbox worker1
    [root@i24 ~]# docker-machine create --driver virtualbox worker2
    
Start the swarm. Swarm be like our machines scheme. host i24 will be the manager of swarm.
Note: Use --advertise-addr because docker detects more than 1 IP available for manage the swarm. Docker detects i24, worker1 and worker2 ip's.

    [root@i24 ~]# docker swarm init --advertise-addr 192.168.2.54
    Swarm initialized: current node (dtmwaya4b7rs2s7aaq52m1zui) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-13uxligqkk899n29shfkr0zifw0rl1fejzxorzvv24zoei3nob-6upa5ri767ir32xb9byrhasd2 192.168.2.54:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

When we start a swarm, Docker prints how we can join workers. Copy and send it (using "docker-machine ssh") to workers vm's.

    [root@i18 ~]# docker swarm join --token SWMTKN-1-13uxligqkk899n29shfkr0zifw0rl1fejzxorzvv24zoei3nob-6upa5ri767ir32xb9byrhasd2 192.168.2.54:2377
    This node joined a swarm as a worker.

    [root@i24 ~]# docker-machine ssh worker1 "docker swarm join --token SWMTKN-1-13uxligqkk899n29shfkr0zifw0rl1fejzxorzvv24zoei3nob-6upa5ri767ir32xb9byrhasd2 192.168.2.54:2377"
    This node joined a swarm as a worker.

    [root@i24 ~]# docker-machine ssh worker2 "docker swarm join --token SWMTKN-1-13uxligqkk899n29shfkr0zifw0rl1fejzxorzvv24zoei3nob-6upa5ri767ir32xb9byrhasd2 192.168.2.54:2377"
    This node joined a swarm as a worker.

    [root@i24 ~]# docker node ls
    ID                            HOSTNAME                               STATUS              AVAILABILITY        MANAGER STATUS
    lxqyzkklcuyn95123qdciwxhb     i18.informatica.escoladeltreball.org   Ready               Active              
    dtmwaya4b7rs2s7aaq52m1zui *   i24.informatica.escoladeltreball.org   Ready               Active              Leader
    j2dwr1fphqre9ob9a6c7d3797     worker1                                Ready               Active              
    6id6wz5eftmb6sakpeyfta8iz     worker2                                Ready               Active              

Now let's deploy the app using "docker stack deploy"

    [root@i24 ~]# docker stack deploy -c docker-compose-cloud.yml cloud
    Creating network cloud_netadri
    Creating network cloud_default
    Creating service cloud_visualizer
    Creating service cloud_portainer
    Creating service cloud_ldap
    Creating service cloud_psql

    [root@i24 ~]# docker stack ls
    NAME                SERVICES
    cloud               4

    [root@i24 ~]# docker stack ps cloud 
    ID                  NAME                 IMAGE                             NODE                                   DESIRED STATE       CURRENT STATE            ERROR               PORTS
    5tlhvyppm7se        cloud_psql.1         xdri97/postgresql:cloud           worker1                                Running             Running 9 seconds ago                        
    mes3c55stizb        cloud_ldap.1         xdri97/ldapserver:cloud           worker2                                Running             Running 11 seconds ago                       
    k6svb7u98ks6        cloud_portainer.1    portainer/portainer:latest        i24.informatica.escoladeltreball.org   Running             Running 14 seconds ago                       
    z3mbco3c06x5        cloud_visualizer.1   dockersamples/visualizer:stable   i24.informatica.escoladeltreball.org   Running             Running 15 seconds ago                       
    m51nfb5iwths        cloud_psql.2         xdri97/postgresql:cloud           i18.informatica.escoladeltreball.org   Running             Running 9 seconds ago                        
    ugn2guraw064        cloud_ldap.2         xdri97/ldapserver:cloud           i18.informatica.escoladeltreball.org   Running             Running 11 seconds ago                       
    ss987rta8j91        cloud_ldap.3         xdri97/ldapserver:cloud           worker1                                Running             Running 11 seconds ago  

    [root@i24 ~]# docker service ls
    ID                  NAME                MODE                REPLICAS            IMAGE                             PORTS
    fl4k1qsme6jh        cloud_visualizer    replicated          1/1                 dockersamples/visualizer:stable   *:8080->8080/tcp
    jrsslyr105ax        cloud_ldap          replicated          3/3                 xdri97/ldapserver:cloud           *:389->389/tcp
    t26x499in84l        cloud_psql          replicated          2/2                 xdri97/postgresql:cloud           *:5432->5432/tcp
    tnv71flc2rrr        cloud_portainer     replicated          1/1                 portainer/portainer:latest        *:9000->9000/tcp


So our services are running ok and we can see or ask them:
* Portainer

![Swarm Portainer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/swarmportainer.png)

* PSQL

To both physical hosts:

    [root@i24 ~]# psql -U docker -h 192.168.2.54 -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

    [root@i24 ~]# psql -U docker -h 192.168.2.48 -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

* LDAP

To all hosts:

    [root@i24 ~]# ldapsearch -x -LLL -h 192.168.2.54 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

    [root@i24 ~]# ldapsearch -x -LLL -h 192.168.2.48 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

    [root@i24 ~]# ldapsearch -x -LLL -h 192.168.99.100 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

    [root@i24 ~]# ldapsearch -x -LLL -h 192.168.99.101 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org
    
* Visualizer

![Swarm Visualizer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/swarmvisualizer.png)

    
    