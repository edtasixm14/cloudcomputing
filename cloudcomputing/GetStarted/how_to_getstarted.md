# Docker "Get Started" own tutorial

Agafo el i24 de l'aula per a instal·lar l'última versió de docker necessaria per a poder fer Swarms (clústers).

    [root@i24 ~]# dnf -y install dnf-plugins-core
    [root@i24 ~]# dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    [root@i24 ~]# dnf config-manager --set-enabled docker-ce-edge
    [root@i24 ~]# dnf config-manager --set-enabled docker-ce-test
    [root@i24 ~]# dnf install docker-ce

Ara comprovo la versió de docker i efectivament tinc la 17.07 necessaria per al projecte.

    [root@i24 ~]# docker --version
    Docker version 17.07.0-ce-rc3, build 665d244
    
Abans de començar res, habilito que docker estigui enchegat quan arrenca la màquina.

    [root@i24 ~]# systemctl start docker
    [root@i24 ~]# systemctl enable docker
    Created symlink from /etc/systemd/system/multi-user.target.wants/docker.service to /usr/lib/systemd/system/docker.service.

Ara faré els tutorials de [docker started](https://docs.docker.com/get-started/#test-docker-installation):

Part 1: Llistar imatges i containers.

    [root@i24 ~]# docker image ls 
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    hello-world         latest              e38bc07ac18e        11 days ago         1.85kB
    [root@i24 ~]# docker container ls
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    [root@i24 ~]# docker container ls -all
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
    ba86472453a5        hello-world         "/hello"            3 minutes ago       Exited (0) 3 minutes ago                       tender_lamport
    [root@i24 ~]# docker container ls -aq
    ba86472453a5

Part 2: [Dockerfile](https://docs.docker.com/get-started/part2/#dockerfile), seguim els passos per crear l'app de python.

    [root@i24 ~]# mkdir pythondocker
    [root@i24 ~]# cd pythondocker/
    [root@i24 pythondocker]# vim Dockerfile
    [root@i24 pythondocker]# vim requirements.txt
    [root@i24 pythondocker]# vim app.py
    [root@i24 pythondocker]# ls
    app.py  Dockerfile  requirements.txt
    [root@i24 pythondocker]# docker build -t friendlyhello .

Ara tenim l'imatge python i la nostra imatge creada com a _friendlyhello_.

    [root@i24 pythondocker]# docker image ls
    REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
    friendlyhello       latest              54a3313935cc        About a minute ago   151MB
    python              2.7-slim            ce007f0fff83        2 days ago           139MB
    hello-world         latest              e38bc07ac18e        11 days ago          1.85kB

Fem run del docker amb l'app:

    [root@i24 pythondocker]# docker run -p 4000:80 friendlyhello
     * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
     
Comprovem que funciona demanant la pàgina amb curl:

    [root@i24 ~]# curl http://localhost:4000
    <h3>Hello World!</h3><b>Hostname:</b> 875ef9f9699a<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    
Ara el run del docker ha rebut la petició

    [root@i24 pythondocker]# docker run -p 4000:80 friendlyhello
    * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
    172.17.0.1 - - [23/Apr/2018 07:49:40] "GET / HTTP/1.1" 200 -
    
També podem encendre l'app en segon pla amb el _detached mode_:

    [root@i24 pythondocker]# docker run -d -p 4000:80 friendlyhello
    dfab0c75aace08f77427c25d51ce987f2313a590b37478c65656869d63249605
    [root@i24 ~]# curl http://localhost:4000
    <h3>Hello World!</h3><b>Hostname:</b> dfab0c75aace<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>

Apagar container (Llistar i fer stop per ID):

    [root@i24 pythondocker]# docker container ls
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                  NAMES
    dfab0c75aace        friendlyhello       "python app.py"     4 minutes ago       Up 4 minutes        0.0.0.0:4000->80/tcp   tender_franklin
    [root@i24 pythondocker]# docker container stop dfab0c75aace
    dfab0c75aace

Ara compartirem a Docker Hub la nostra imatge creada.

Fem login:

    [root@i24 pythondocker]# docker login
    Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
    Username: xdri97
    Password: 
    Login Succeeded

Creem el tag amb _dockeruser/nomimatge_

    [root@i24 pythondocker]# docker tag friendlyhello:latest xdri97/pythondocker
    [root@i24 pythondocker]# docker image ls
    REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
    friendlyhello         latest              54a3313935cc        31 minutes ago      151MB
    xdri97/pythondocker   latest              54a3313935cc        31 minutes ago      151MB

Pujem la imatge a DockerHub:

    [root@i24 pythondocker]# docker push xdri97/pythondocker

A partir d'ara podem fer run de la imatge possant el repositori de DockerHub

    [root@i24 pythondocker]# docker run -p 4000:80 xdri97/pythondocker
    * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)

Part 3: Serveis. Escalem la nostra aplicació i habilitem l'equilibri de càrrega.

Per aquesta part necesitem el docker compose, l'installem de la seguent manera:

    [root@i24 ~]# docker-compose --version
    -bash: /usr/local/bin/docker-compose: No such file or directory
    [root@i24 ~]# curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0    813      0 --:--:-- --:--:-- --:--:--   812
    100 10.3M  100 10.3M    0     0  2174k      0  0:00:04  0:00:04 --:--:-- 3376k
    [root@i24 ~]# chmod +x /usr/local/bin/docker-compose
    [root@i24 ~]# docker-compose --version
    docker-compose version 1.21.0, build 5920eb0

Els serveis són en realitat només "contenidors en producció". Un servei només executa una imatge, però codifica la forma en què s'executa la imatge: quins ports hauria d'utilitzar, quantes rèpliques del contenidor haurien d'executar perquè el servei tingui la capacitat que necessiti i així successivament.

Creem el docker-compose.yml amb la imatge creada abans.

Amb els seguents passos farem que la nostra app funcioni amb equilibri de càrrega.

    [root@i24 ~]# docker swarm init
    [root@i24 ~]# docker stack deploy -c docker-compose.yml adripyapp
    Creating network adripyapp_webnet
    Creating service adripyapp_web
    [root@i24 ~]# docker service ls
    ID                  NAME                MODE                REPLICAS            IMAGE                        PORTS
    kkz1ooswb41f        adripyapp_web       replicated          5/5                 xdri97/pythondocker:latest   *:80->80/tcp
    [root@i24 ~]# docker service ps adripyapp_web 
    ID                  NAME                IMAGE                        NODE                                   DESIRED STATE       CURRENT STATE                ERROR               PORTS
    pov443c19ms4        adripyapp_web.1     xdri97/pythondocker:latest   i24.informatica.escoladeltreball.org   Running             Running about a minute ago                       
    ilczst8cqkpw        adripyapp_web.2     xdri97/pythondocker:latest   i24.informatica.escoladeltreball.org   Running             Running about a minute ago                       
    zlzwdaya6ftf        adripyapp_web.3     xdri97/pythondocker:latest   i24.informatica.escoladeltreball.org   Running             Running about a minute ago                       
    fhudyg06f3bm        adripyapp_web.4     xdri97/pythondocker:latest   i24.informatica.escoladeltreball.org   Running             Running about a minute ago                       
    ahao32u60rs6        adripyapp_web.5     xdri97/pythondocker:latest   i24.informatica.escoladeltreball.org   Running             Running about a minute ago  

Fent peticions amb curl o amb el navegador veiem com el hostname que ens està atenent canvia :

    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> ae9e4968c803<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> 8bfd833e910a<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> 144746fff590<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> b1fcc2c80a02<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> 2de1e85d2e4a<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    [root@i24 ~]# curl -4 http://localhost
    <h3>Hello World!</h3><b>Hostname:</b> ae9e4968c803<br/><b>Visits:</b> <i>cannot connect to Redis, counter disabled</i>
    
Apagar l'stack i el swarm.

    [root@i24 ~]# docker stack rm adripyapp 
    Removing service adripyapp_web
    Removing network adripyapp_webnet
    [root@i24 ~]# docker swarm leave --force 
    Node left the swarm.
    
Part 4: Swarms

En aquesta part despleguem aquesta aplicació en un clúster, executada en diverses màquines. Les aplicacions multi-contenidor i multi-màquina són possibles unint màquines múltiples en un clúster "Dockeritzat" anomenat "Swarm".

Primer haurem de tenir tot l'anterior instal·lat + docker-machine y virtualbox per a crear màquines "reals":

    [root@i24 ~]# docker-machine version
    bash: docker-machine: command not found...
    [root@i24 ~]# base=https://github.com/docker/machine/releases/download/v0.14.0 && curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine
      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0   1056      0 --:--:-- --:--:-- --:--:--  1058
    100 26.7M  100 26.7M    0     0  2658k      0  0:00:10  0:00:10 --:--:-- 3628k
    [root@i24 ~]# docker-machine versiondocker-machine version 0.14.0, build 89b8332
    [root@i24 ~]# wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo -O /etc/yum.repos.d/virtualbox.repo
    [root@i24 ~]# dnf -y install VirtualBox-5.1
    
Actualitzem el kernel perque VBox pugui ser utilitzat correctament (si no tindrem un error)

    [root@i24 ~]# dnf install -y kernel-headers kernel-devel dkms gcc
    [root@i24 ~]# /sbin/vboxconfig

Ara fem les VM.

    [root@i24 ~]# docker-machine create --driver virtualbox myvm1
    [root@i24 ~]# docker-machine create --driver virtualbox myvm2
    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   

Aquestes màquines virtuals passen a status "stopped" quan el ost s'apaga. Pen encendre-les utilitzem el seguent:

    docker-machine start myvm1
    docker-machine start myvm2

Continuo amb el tutorial, enchego les màquines virtuals i veiem les IP:

    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce 

Creem el Swarm:

    [root@i24 ~]# docker-machine --native-ssh ssh myvm1 "docker swarm init --advertise-addr 192.168.99.100"
    Swarm initialized: current node (vmzrdo3t5gt4k05xjsfzpyhga) is now a manager.

    To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-228u9ys612pkgw37ia111klhiuvex10w9u30fpodgux4lqllnb-7arxmaopalez0y73tjfu81z34 192.168.99.100:2377

    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
    
Fem que la vm2 sigui un worker del swarm:

    [root@i24 ~]# docker-machine ssh myvm2 "docker swarm join \
    --token SWMTKN-1-228u9ys612pkgw37ia111klhiuvex10w9u30fpodgux4lqllnb-7arxmaopalez0y73tjfu81z34 \
    192.168.99.100:2377"
    This node joined a swarm as a worker.

Per veure l'estat dels nodes del swarm he de fer "docker node ls" al manager del Swarm:

    [root@i24 ~]# docker-machine ssh myvm1 "docker node ls"
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    vmzrdo3t5gt4k05xjsfzpyhga *   myvm1               Ready               Active              Leader              18.04.0-ce
    sli9vzg2q9mupw3dljt61f1o8     myvm2               Ready               Active                                  18.04.0-ce

*Nomes hem de fer ordres docker contra el manager*
Per a poder utilitzar el docker-compose.yml com abans el recomendat es configurar l'env per parlar contra el deamon docker de la vm.

    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   
    [root@i24 ~]# docker-machine env myvm1
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_HOST="tcp://192.168.99.100:2376"
    export DOCKER_CERT_PATH="/root/.docker/machine/machines/myvm1"
    export DOCKER_MACHINE_NAME="myvm1"
    # Run this command to configure your shell: 
    # eval $(docker-machine env myvm1)
    [root@i24 ~]# eval $(docker-machine env myvm1)
    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce  

Veiem que ara l'actiu es el manager del swarm, ara podem fer el "docker stack deploy" d'abans.
_el init del swarm no cal perque ya l'hem fet abans dins de la myvm1_

    [root@i24 ~]# docker stack deploy -c docker-compose.yml adripyappswarm
    Creating network adripyappswarm_webnet
    Creating service adripyappswarm_web
    [root@i24 ~]# docker service ls
    ID                  NAME                 MODE                REPLICAS            IMAGE                        PORTS
    zz40frjsekzx        adripyappswarm_web   replicated          5/5                 xdri97/pythondocker:latest   *:80->80/tcp
    [root@i24 ~]# docker service ps adripyappswarm_web 
    ID                  NAME                   IMAGE                        NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
    f32tfgwvzztd        adripyappswarm_web.1   xdri97/pythondocker:latest   myvm2               Running             Running 53 seconds ago                       
    5cdzhggo67p6        adripyappswarm_web.2   xdri97/pythondocker:latest   myvm1               Running             Running 49 seconds ago                       
    mok5ohmmyn63        adripyappswarm_web.3   xdri97/pythondocker:latest   myvm2               Running             Running 53 seconds ago                       
    hnawjlw91frs        adripyappswarm_web.4   xdri97/pythondocker:latest   myvm1               Running             Running 49 seconds ago                       
    kp83ypnca6dg        adripyappswarm_web.5   xdri97/pythondocker:latest   myvm2               Running             Running 52 seconds ago  

*Comandes per parlar amb vm's*

    docker-machine env <machine>
    docker-machine ssh <machine> "<command>"
    docker-machine scp <file> <machine>:~
    
Ara eliminem l'stack:

    [root@i24 ~]# docker stack rm adripyappswarm 
    Removing service adripyappswarm_web
    Removing network adripyappswarm_webnet

En aquest punt també podriem fer que les màquines no formin part del swarm, es faria de la seguent forma pero no ho farem per utilitzar-ho a la part 5:

    docker-machine ssh myvm2 "docker swarm leave"
    docker-machine ssh myvm1 "docker swarm leave --force"

Treiem l'enviroment que feia que les ordres docker fosin contra el manager ( ara no hi ha cap "active"):

    [root@i24 ~]# eval $(docker-machine env -u)
    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   

Part 5: Stacks

Hem de tenir tot l'anterior preparat i "ready", el swarm sobretot. Ho comprovem:

    [root@i24 ~]# docker-machine ssh myvm1 "docker node ls"
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    vmzrdo3t5gt4k05xjsfzpyhga *   myvm1               Ready               Active              Leader              18.04.0-ce
    sli9vzg2q9mupw3dljt61f1o8     myvm2               Ready               Active                                  18.04.0-ce

Fem un nou docker-compose amb l'extra del visualizer al port 8080
Configurem per parlar amb el manager un altre cop:

    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   
    [root@i24 ~]# docker-machine env myvm1
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_HOST="tcp://192.168.99.100:2376"
    export DOCKER_CERT_PATH="/root/.docker/machine/machines/myvm1"
    export DOCKER_MACHINE_NAME="myvm1"
    # Run this command to configure your shell: 
    # eval $(docker-machine env myvm1)
    [root@i24 ~]# eval $(docker-machine env myvm1)
    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   

Executem l'stack

    [root@i24 ~]# docker stack deploy -c docker-compose2.yml pythonvisualizer
    Creating network pythonvisualizer_webnet
    Creating service pythonvisualizer_web
    Creating service pythonvisualizer_visualizer

Tenim una única copia de "visualizer" treballant i les 5 replicas donant servei.

    [root@i24 ~]# docker stack ps pythonvisualizer 
    ID                  NAME                            IMAGE                             NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
    otrpe75kqo9o        pythonvisualizer_visualizer.1   dockersamples/visualizer:stable   myvm1               Running             Running 2 minutes ago                       
    xt8h6zpp3hmj        pythonvisualizer_web.1          xdri97/pythondocker:latest        myvm1               Running             Running 3 minutes ago                       
    w49u5dspe35t        pythonvisualizer_web.2          xdri97/pythondocker:latest        myvm2               Running             Running 3 minutes ago                       
    tyqojod9a6uy        pythonvisualizer_web.3          xdri97/pythondocker:latest        myvm1               Running             Running 3 minutes ago                       
    qjaznmzciltb        pythonvisualizer_web.4          xdri97/pythondocker:latest        myvm2               Running             Running 3 minutes ago                       
    wesp0jya7jbu        pythonvisualizer_web.5          xdri97/pythondocker:latest        myvm2               Running             Running 3 minutes ago 

Editem el compose per afegir el servei de Redis, creem la carpeta de "data" introduida al docker-compose.yml i comprovem que parlem amb el manager:

    [root@i24 ~]# docker-machine ssh myvm1 "mkdir ./data"
    [root@i24 ~]# docker-machine ls
    NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myvm1   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce  

Fem l'stack de nou i veiem com tot funciona correctament:

    [root@i24 ~]# docker stack deploy -c docker-compose3.yml redisready
    Creating network redisready_webnet
    Creating service redisready_redis
    Creating service redisready_web
    Creating service redisready_visualizer
    [root@i24 ~]# docker service ls
    ID                  NAME                    MODE                REPLICAS            IMAGE                             PORTS
    hye5kkk6ezww        redisready_web          replicated          5/5                 xdri97/pythondocker:latest        *:80->80/tcp
    lyzdvy8yz0jz        redisready_visualizer   replicated          1/1                 dockersamples/visualizer:stable   *:8080->8080/tcp
    mweo8hpk09ap        redisready_redis        replicated          1/1                 redis:latest                      *:6379->6379/tcp
    [root@i24 ~]# docker stack ps redisready
    ID                  NAME                      IMAGE                             NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
    t7bxo7ethp5o        redisready_visualizer.1   dockersamples/visualizer:stable   myvm1               Running             Running 16 seconds ago                       
    413qdbndv9yx        redisready_web.1          xdri97/pythondocker:latest        myvm2               Running             Running 28 seconds ago                       
    ddk0gphxk3sy        redisready_redis.1        redis:latest                      myvm1               Running             Running 21 seconds ago                       
    ahug0h7jacio        redisready_web.2          xdri97/pythondocker:latest        myvm2               Running             Running 28 seconds ago                       
    ys4lsuzc9xc8        redisready_web.3          xdri97/pythondocker:latest        myvm1               Running             Running 18 seconds ago                       
    y3nl0le0teh8        redisready_web.4          xdri97/pythondocker:latest        myvm2               Running             Running 28 seconds ago                       
    k4s6akksn1uk        redisready_web.5          xdri97/pythondocker:latest        myvm1               Running             Running 18 seconds ago 

Part 6: "Desplegar" l'app.

Ja tenint un swarm and AWS:
    
    [root@i24 ~]# docker run --rm -ti -v /var/run/docker.sock:/var/run/docker.sock -e DOCKER_HOST dockercloud/client xdri97/test
    => You can now start using the swarm xdri97/test by executing:
    	export DOCKER_HOST=tcp://127.0.0.1:32769
    [root@i24 ~]# export DOCKER_HOST=tcp://127.0.0.1:32769
    [root@i24 ~]# docker node ls
    ID                            HOSTNAME                                      STATUS              AVAILABILITY        MANAGER STATUS
    vc7ux4vmkd883gurl43fmf1oq *   ip-172-31-2-56.eu-west-2.compute.internal     Ready               Active              Leader
    5xf33a4f645w0mlmj1aexlxw2     ip-172-31-8-165.eu-west-2.compute.internal    Ready               Active              
    xka4kgfr0ebykci2641gaav25     ip-172-31-17-140.eu-west-2.compute.internal   Ready               Active   
    ------------------------------------------------------------------------------------------------------------------------------------
    


PAREM TOT:

    [root@i24 ~]# docker stack rm redisready
    Removing service redisready_web
    Removing service redisready_visualizer
    Removing service redisready_redis
    Removing network redisready_webnet
    [root@i24 ~]# docker-machine ssh myvm2 "docker swarm leave"
    Node left the swarm.
    [root@i24 ~]# docker-machine ssh myvm1 "docker swarm leave --force"
    Node left the swarm.
    [root@i24 ~]# eval $(docker-machine env -u)
    [root@i24 ~]# docker-machine rm myvm1
    About to remove myvm1
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed myvm1
    [root@i24 ~]# docker-machine rm myvm2
    About to remove myvm2
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed myvm2
    [root@i24 ~]# docker-machine ls
    NAME   ACTIVE   DRIVER   STATE   URL   SWARM   DOCKER   ERRORS

